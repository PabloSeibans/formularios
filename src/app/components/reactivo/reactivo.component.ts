import { Component, OnInit } from '@angular/core';
import { AbstractControlOptions, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidadoresService } from 'src/app/servicios/validadores.service';

@Component({
  selector: 'app-reactivo',
  templateUrl: './reactivo.component.html',
  styleUrls: ['./reactivo.component.css']
})
export class ReactivoComponent implements OnInit {

  forma!: FormGroup;

  constructor(private fb: FormBuilder,
              private validadores: ValidadoresService) {
    this.crearFormulario();
    //this.cargarDataAlFormulario();
    this.cargarDataAlFormulario2();
   }

  ngOnInit(): void {
  }

  get nombreNoValido(){
    return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched;
  }
  get apellidoNoValido(){
    return (this.forma.get('apellido')?.invalid && 
          !this.forma.get('apellido')?.errors?.['noZaralai']) && 
          this.forma.get('apellido')?.touched;
  }
  get correoNoValido(){
    return this.forma.get('correo')?.invalid && this.forma.get('correo')?.touched;
  }
  get distritoNoValido(){
    return this.forma.get('direccion.distrito')?.invalid && this.forma.get('direccion.distrito')?.touched;
  }
  get ciudadNoValido(){
    return this.forma.controls['direccion'].get('ciudad')?.invalid && this.forma.controls['direccion'].get('ciudad')?.touched;
  }
  get pasatiempos2(){
    return this.forma.get('pasatiempos') as FormArray;
  }

  get apellidoNoZaralai(){
    return this.forma.get('apellido')?.errors?.['noZaralai'];
  }

  get pass1NoValido(){
    return this.forma.get('pass1')?.invalid && this.forma.get('pass1')?.touched;
  }

  get pass2NoValido(){
    const pass1 = this.forma.get('pass1')?.value;
    const pass2 = this.forma.get('pass2')?.value;

    return (pass1 === pass2) ? false : true;
  }

  get usuarioNoValido(){
    return (this.forma.get('usuario')?.invalid && !this.forma.get('usuario')?.errors?.['existe']) && this.forma.get('usuario')?.touched;
  }

  get usuarioIgualJerjes() {
    return this.forma.get('usuario')?.errors?.['existe'];
  }

  crearFormulario(): void {
    this.forma = this.fb.group({
      //Valores del Array:
      //1er valor: El valor por defecto que tendra
      //Es el primer valor por defecto que introducimos en la caja de texto
      //2do valor: Son los validadores sincronos
      //En el 2do tenemos los valores que podemos poner expresiones regulares
      //3er valor: Son los validadores asincronos
      //Aca son los endpoints y web api rest
      nombre: ['', [Validators.required, Validators.minLength(4)]],
      //Si solo voy a usar un validator no requiere que use en un array
      apellido: ['', [Validators.required, Validators.minLength(4), Validators.pattern(/^[a-zA-ZñÑ\s]+$/), this.validadores.noZaralai]],
      correo: ['',[Validators.required, Validators.pattern(/^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/)]],
      pass1: ['', Validators.required],
      pass2: ['', Validators.required],
      usuario: ['', Validators.required, this.validadores.existeUsuario],
      direccion: this.fb.group({
        distrito:['', Validators.required],
        ciudad: ['',Validators.required]
      }),
      pasatiempos: this.fb.array([
        [], []
      ])
    }, {
      validators: this.validadores.passwordsIguales('pass1', 'pass2')
    } as AbstractControlOptions);
  }

  cargarDataAlFormulario(): void{
    this.forma.setValue({
      nombre: 'Juan',
      apellido: 'Perez',
      correo: 'juan@gmail.com',
      direccion: {
        distrito: 'Central',
        ciudad: 'Oruro'
      }
    })
  }

  cargarDataAlFormulario2():void {
    // Este es el que mas se usa para cambiar datos en un formulario
    this.forma.patchValue({
      apellido: 'Perez'
    });
  }

  guardar():void {
    console.log(this.forma.value);
    //Reset del formulario
    this.limpiarFormulario();
  }

  limpiarFormulario():void{

    //Limpia todo lo que tiene la forma
    //this.forma.reset();
    this.forma.reset({
      nombre: 'Pedro'
    });
  }

  agregarpasatiempo(): void{
    // lo que hace es ir a pasatiempos 2 y lo que retorna es un array en el cual yo puedo hacer push
    //this.pasatiempos2.push(this.fb.control('Nuevo elemento', Validators.required));
    this.pasatiempos2.push(this.fb.control(''));
  }

  borrarPasatiempo(i: number): void{
    this.pasatiempos2.removeAt(i);
  }
}
